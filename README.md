<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

<p align="center">
<a href="https://github.com/laravel/framework/actions"><img src="https://github.com/laravel/framework/workflows/tests/badge.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

You may also try the [Laravel Bootcamp](https://bootcamp.laravel.com), where you will be guided through building a modern Laravel application from scratch.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains thousands of video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.

## Laravel Sponsors

We would like to extend our thanks to the following sponsors for funding Laravel development. If you are interested in becoming a sponsor, please visit the [Laravel Partners program](https://partners.laravel.com).

### Premium Partners

- **[Vehikl](https://vehikl.com/)**
- **[Tighten Co.](https://tighten.co)**
- **[WebReinvent](https://webreinvent.com/)**
- **[Kirschbaum Development Group](https://kirschbaumdevelopment.com)**
- **[64 Robots](https://64robots.com)**
- **[Curotec](https://www.curotec.com/services/technologies/laravel/)**
- **[Cyber-Duck](https://cyber-duck.co.uk)**
- **[DevSquad](https://devsquad.com/hire-laravel-developers)**
- **[Jump24](https://jump24.co.uk)**
- **[Redberry](https://redberry.international/laravel/)**
- **[Active Logic](https://activelogic.com)**
- **[byte5](https://byte5.de)**
- **[OP.GG](https://op.gg)**

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Code of Conduct

In order to ensure that the Laravel community is welcoming to all, please review and abide by the [Code of Conduct](https://laravel.com/docs/contributions#code-of-conduct).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).



-------------------------------------------

necesita gd y zip extension de php


Prueba técnica y de conocimientos - Desarrollador de software Laravel, Jquery y MySql 
1) Deberás realizar un CRUD de contribuyentes, teniendo en cuenta la  siguiente información: 
Tipo documento 
Documento 
Nombres 
Apellidos 
En el caso de que el tipo de documento sea NIT, se debe cambiar los  campos nombres y apellidos por razón social. Además, se tiene que  crear un algoritmo que permita identificar cuando se almacena en  nombres y apellidos la información, dependiendo de los espacios  que contengan las palabras, ejemplo:  
Bello Renacer SAS 
Nombres: Bello 
Apellidos: Renacer SAS 
Instrumentos Odontológicos Sonría SAS 
Nombres: Instrumentos Odontológicos 
Apellidos: Sonría SAS 
No se debe crear un campo nuevo llamado razón social. 
Nombre completo; (debe guardarse mediante el modelo de  contribuyente).  
Dirección 
Teléfono 
Celular

Email; (Es esencial validar los correos electrónicos para asegurar  que sean correctos y funcionales antes de guardarlos. Crear un  helper en el backend o desde el cliente validar la estructura del  correo a guardar). 
Usuario 
Fecha del sistema 
El CRUD de contribuyentes, debe contar con filtros estrictos para cada  campo o columna que se muestre en la tabla principal. Además, de los  botones como crear, editar, eliminar y ver información, si no soy súper  usuario no puedo realizar ninguna acción del crud, solo ver la tabla. 
Los campos que serán visibles en la tabla son:  
Tipo documento, documento, nombres y apellidos, y teléfono, los campos  restantes deben visualizarse en el show, puede ser una vista o un modal. 
2) (Lógica de programación): Utilizando una función recursiva debe  calcular cuantas letras tiene los campos nombres y apellidos de cada  contribuyente, ejemplo: 
Nombres: PEDRO Apellidos: PINEDA 
P:2 
E:2 
D:2 
R:1 
O:1 
I:1 
N:1 
A:1 
Se debe hacer en el backend mediante un helper, y la información se  entrega desde el controlador a la vista.

3) Para realizar la entrega, debe contar con un login y un CRUD adicional  de usuarios, donde se puedan crear con dos roles (Administrador y  super usuario). El select de rol, se alimenta de la tabla roles.

------------------------------------------------------------

Summary:

users have roles
roles have permissions
app always checks for permissions (as much as possible), not roles
views check permission-names
policies check permission-names
model policies check permission-names
controller methods check permission-names
middleware check permission names, or sometimes role-names
routes check permission-names, or maybe role-names if you need to code that way.

------------------------------------------------------------------
Guard: web
+-----------------+-------+--------+------------+
|                 | Admin | Normal | SuperAdmin |
+-----------------+-------+--------+------------+
| create doctype  |  ·    |  ·     |  ✔         |
| create taxpayer |  ✔    |  ·     |  ✔         |
| create user     |  ·    |  ·     |  ✔         |
| delete doctype  |  ·    |  ·     |  ✔         |
| delete taxpayer |  ✔    |  ·     |  ✔         |
| delete user     |  ·    |  ·     |  ✔         |
| edit doctype    |  ·    |  ·     |  ✔         |
| edit taxpayer   |  ✔    |  ·     |  ✔         |
| edit user       |  ·    |  ·     |  ✔         |
| view doctype    |  ✔    |  ✔     |  ✔         |
| view taxpayer   |  ✔    |  ✔     |  ✔         |
| view user       |  ·    |  ✔     |  ✔         |
+-----------------+-------+--------+----------