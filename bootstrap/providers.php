<?php

return [
    App\Providers\AppServiceProvider::class,
    Yajra\DataTables\DataTablesServiceProvider::class,
    // ... other providers ...

    Yajra\DataTables\ButtonsServiceProvider::class,
    Yajra\DataTables\FractalServiceProvider::class,
    Yajra\DataTables\HtmlServiceProvider::class,
    Yajra\DataTables\EditorServiceProvider::class,
    Yajra\DataTables\Bootstrap4ServiceProvider::class,
    RealRashid\SweetAlert\SweetAlertServiceProvider::class,
    Spatie\Permission\Laravel\PermissionServiceProvider::class,

];

