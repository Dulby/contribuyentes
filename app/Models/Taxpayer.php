<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Taxpayer extends Model
{
    use HasFactory;

    public function documentType()
    {
        return $this->belongsTo(DocumentType::class, 'doc_type', 'short_name'); // Specify custom foreign and local key columns
        //return $this->belongsTo(DocumentType::class, 'short_name');
    }
}
