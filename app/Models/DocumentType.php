<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DocumentType extends Model
{
    use HasFactory;

    public function users()
    {
        return $this->hasMany(Taxpayer::class, 'short_name', 'doc_type'); // Specify custom foreign and local key columns
        //return $this->hasMany(Taxpayer::class, 'short_name', 'doc_type');
    }
    
}
