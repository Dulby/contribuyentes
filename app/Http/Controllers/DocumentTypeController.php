<?php

namespace App\Http\Controllers;

use App\Models\DocumentType;
use Illuminate\Http\Request;

class DocumentTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('doc-type.list')->with('documentTypes', DocumentType::all());
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('doc-type.list');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        return view('doc-type.list');
    }

    /**
     * Display the specified resource.
     */
    public function show(DocumentType $documentType)
    {
        return view('doc-type.form')
            ->with('docType', $documentType)
            ->with('read_only', true);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(DocumentType $documentType)
    {
        return view('doc-type.form')
            ->with('docType', $documentType);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, DocumentType $documentType)
    {
        return view('doc-type.list');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(DocumentType $documentType)
    {
        return view('doc-type.list');
    }
}
