<?php

namespace App\Http\Controllers;

use App\Models\Taxpayer;
use App\Models\DocumentType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Flash;
use Illuminate\Support\Facades\Validator;

class TaxpayerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('taxpayers.list')
            ->with('doctype', $this->getDocTypeSelect())
            ->with('taxpayers', Taxpayer::with('documentType')->get());
    }
    
    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('taxpayers.form')
                ->with('doctype', $this->getDocTypeSelect());
    }

    /**
     * Store a newly created resource in storage.
     */
    public function storeOrUpdate(Request $request)
    {
        try {
            $taxpayer = ($request->input('id')!='')
                        ? Taxpayer::find($request->input('id'))
                        : new Taxpayer();

            $validatedData = $request->validateWithBag('taxpayer', [
                'doc_type' => ['required', 'string', 'max:5'],
                'doc_number' => ['unique:taxpayers,doc_number,'. $request->input('id')],// Ensure unique document number except this record
                'names' => ['required', 'string', 'max:30'],
                'address' => ['string'],
                'phone' => ['nullable', 'string'],
                'email' => ['nullable', 'email'],
            ]);
            /* 
            doc_type doc_number names lastnames address phone phone secondary_phone email
            */
    
            $taxpayer->doc_type = $validatedData['doc_type'];
            $taxpayer->doc_number = $validatedData['doc_number'];
            $taxpayer->names = $validatedData['names'];
            $taxpayer->lastnames = $validatedData['lastnames'] ?? '';
            $taxpayer->address = $validatedData['address'];
            $taxpayer->phone = $validatedData['phone'];
            $taxpayer->email = $validatedData['email'];
    
            $taxpayer->save();
    
            return redirect()->route('taxpayer.index');
        } catch (\Throwable $th) {
            return back()->withErrors(['error' => $th->getMessage()]);
        }
    }


    /**
     * Display the specified resource.
     */
    public function show(Taxpayer $taxpayer)
    {
        return view('taxpayers.form')
            ->with('taxpayer', $taxpayer)
            ->with('doctype', $this->getDocTypeSelect())
            ->with('read_only', true);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Taxpayer $taxpayer)
    {
        return view('taxpayers.form')
            ->with('doctype', $this->getDocTypeSelect())
            ->with('taxpayer', $taxpayer);
    }

    /**
     * Update the specified resource in storage.
     */


    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Taxpayer $taxpayer)
    {
        $taxpayer->delete();
        return redirect()->route('taxpayer.index');
    }

    private function getDocTypeSelect() {
        $document_types = DocumentType::all();
        $document_types_select = [];
        foreach ($document_types as $document_type) {
            $document_types_select[$document_type->short_name] = $document_type->name;
        }
        return $document_types_select;
    }

}
