<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\RoleHasPermission;
use App\Models\Role;
use App\Models\Permission;

class RolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $roles = Role::all();
        $permissions = Permission::all();
        
        foreach ($roles as $role) {
            foreach ($permissions as $permission) {
                if (($role->name == 'Admin') && (strpos($permission->name, 'taxpayer') !== false)) {
                    RoleHasPermission::create([
                        'role_id' => $role->id,
                        'permission_id' => $permission->id,
                    ]);
                } elseif(($role->name == 'Normal') && (strpos($permission->name, 'view') !== false)) {
                    RoleHasPermission::create([
                        'role_id' => $role->id,
                        'permission_id' => $permission->id,
                    ]);
                } elseif ($role->name == "Super Admin") {
                    RoleHasPermission::create([
                        'role_id' => $role->id,
                        'permission_id' => $permission->id,
                    ]);
                }
            }
        }
    }
}
