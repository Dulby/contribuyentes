<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\Taxpayer;

class TaxpayerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $faker = Faker::create();

        for ($i = 0; $i < 100; $i++) {
            Taxpayer::create([
                'doc_type' => $faker->randomElement(['PS','TI','RC','PP','CC','CE','PPT','PEP','NIT']),
                'doc_number' => $faker->randomNumber(9),
                'names' => $faker->name,
                'lastnames' => $faker->lastName,
                'address' => $faker->address,
                'phone' => $faker->phoneNumber,
                'email' => $faker->safeEmail,
            ]);
        }
    }
}
