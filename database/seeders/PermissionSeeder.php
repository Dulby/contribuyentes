<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Permission::create(['name'=> 'create taxpayer', 'guard_name'=>'web'])
            ->create(['name'=> 'create user', 'guard_name'=>'web'])
            ->create(['name'=> 'delete taxpayer', 'guard_name'=>'web'])
            ->create(['name'=> 'delete user', 'guard_name'=>'web'])
            ->create(['name'=> 'edit taxpayer', 'guard_name'=>'web'])
            ->create(['name'=> 'edit user', 'guard_name'=>'web'])
            ->create(['name'=> 'view taxpayer', 'guard_name'=>'web'])
            ->create(['name'=> 'view user', 'guard_name'=>'web']);
    }
}
