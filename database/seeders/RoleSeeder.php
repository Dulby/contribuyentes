<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Role::create(['name' => 'SuperAdmin', 'guard_name' => 'web'])
        ->create(['name' => 'Admin', 'guard_name' => 'web'])
        ->create(['name' => 'Normal', 'guard_name' => 'web']);
        
    }
}
