<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\DocumentType;

class DocumentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $documentTypesData = [
            'PS' => 'Pasaporte',
            'TI' => 'Tarjeta de identidad',
            'RC' => 'Registro Civil',
            'PP' => 'Permiso de Pasaporte',
            'CC' => 'Cedula de Ciudadania',
            'CE' => 'Cedula de Extranjería',
            'PPT' => 'Permiso de Protección',
            'PEP' => 'Permiso Especial',
            'NIT' => '(NIT) Número de Identificación Tributaria',
        ];

        foreach ($documentTypesData as $short_name => $name) {
            DocumentType::create([
                'short_name' => $short_name,
                'name' => $name,
            ]);
        }
    }
}
