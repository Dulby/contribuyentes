<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * 
     * @return void
     */
    public function run(): void
    {
        
        // Create the admin user
        $user = User::create([
            'name' => 'Super Admin',
            'email' => 'superadmin@example.com',
            'password' => bcrypt('password'),
            'email_verified_at' => now()
        ]);
        $user->assignRole('SuperAdmin');

        $user = User::create([
            'name' => 'Admin',
            'email' => 'admin@example.com',
            'password' => bcrypt('password'),
            'email_verified_at' => now()
        ]);
        $user->assignRole('Admin');

        $user = User::create([
            'name' => 'Normal',
            'email' => 'normal@example.com',
            'password' => bcrypt('password'),
            'email_verified_at' => now()
        ]);
        $user->assignRole('Normal');

    }
}
