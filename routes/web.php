<?php

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\RolesController;
use App\Http\Controllers\TaxpayerController;
use App\Http\Controllers\DocumentTypeController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
})->name('welcome');


Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

/* Route::group(['middleware' => ['auth:admin']], function () {
    // Rutas protegidas para el guardia 'admin'
}); */


Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');

    Route::resource('/user', RolesController::class)->except(['store', 'update']);
    Route::match(['post', 'put'], '/user', [RolesController::class, 'storeOrUpdate'])->name('user.storeOrUpdate');

    Route::resource('/taxpayer', TaxpayerController::class)->except(['store', 'update']);
    Route::match(['post', 'put'], '/taxpayer', [TaxpayerController::class, 'storeOrUpdate'])->name('taxpayer.storeOrUpdate');
    
    Route::resource('/document-type', DocumentTypeController::class);
});

require __DIR__.'/auth.php';
