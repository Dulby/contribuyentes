<script>

    const docType = document.getElementById('doc_type');
    const readOnly = document.getElementById('read_only');

    docType.addEventListener('change', (event) => {
        const selectedOption = event.target.value;
        console.log('Selected option:', selectedOption);

        if (docType.value === 'NIT') {
            console.log('se seleccionó NIT');
        }
    })



</script>

<script type="module">

    $(document).ready(function () {
        if ($('#read_only').val() == true) {
            $('select').prop('disabled', true);
            $('input').prop('disabled', true);
        }
        console.log($("#doc_type").val());
        if ($("#doc_type").val() === "NIT") {
            $('#label_names').text("{{__('taxpayers.business_name')}}")
            $('#names_div').removeClass('col-6');
            $('#names_div').addClass('col-12');

            if ($('#lastnames').val() != '') {
                $('#names').val($('#names').val() + ' ' + $('#lastnames').val());
            }

            $('#lastnames').hide();
            $('#label_lastnames').hide();
        }
    });

    $("#doc_type").change(function () {
        var docType = $(this).val();

        if (docType === "NIT") {
            $('#label_names').text("{{__('taxpayers.business_name')}}")
            $('#names_div').removeClass('col-6');
            $('#names_div').addClass('col-12');
            $('#lastnames').hide();
            $('#label_lastnames').hide();
        } else {
            $('#label_names').text("{{__('taxpayers.names')}}")
            $('#names_div').addClass('col-6');
            $('#names_div').removeClass('col-12');
            $('#lastnames').show();
            $('#label_lastnames').show();
        }
    });

</script>