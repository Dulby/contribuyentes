<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl leading-tight">
            {{ __('taxpayers.taxpayers') }}
        </h2>
    </x-slot>
    <section>
        <div class="py-12">
            <div class=" pt-3 max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6">
                <div class="p-4 sm:p-8 bg-primary text-white shadow sm:rounded-lg">
                    <header>
                        <h2 class="text-lg font-medium text-white">
                            {{ __('taxpayers.taxpayers') }}
                        </h2>
                    </header>

                    @can('create taxpayer')
                    <div class="d-flex justify-content-end">
                        <x-link-button href="{{ route('taxpayer.create') }}">
                            {{ __('Add') }}
                        </x-link-button>
                    </div>
                    @endcan

                    <div class="py-6">
                        <input type="hidden" id="role" value="{{ Auth::user()->getRoleNames() }}" >
                        <hr>
                        <table id="taxpayersTable" class="table table-striped table-secondary" style="width:100%">
                            <thead>
                                <tr>
                                    <th> {{__('taxpayers.docType')}} </th>
                                    <th> {{__('taxpayers.docNumber')}} </th>
                                    <th> {{__('taxpayers.names')}} </th>
                                    <th> {{__('taxpayers.lastnames')}} </th>
                                    <th> {{__('taxpayers.phone')}} </th>
                                    <th> {{__('Actions')}} </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($taxpayers as $taxpayer)
                                <tr>
                                    <td> {{ $taxpayer->documentType->name }} </td>
                                    <td> {{ $taxpayer->doc_number }} </td>
                                    <td> {{ $taxpayer->names }} </td>
                                    <td> {{ $taxpayer->lastnames }} </td>
                                    <td> {{ $taxpayer->phone}} </td>
                                    <td>
                                        <div class="d-flex justify-content-between align-items-center">
                                            @can('view taxpayer')
                                            <x-link-button href="{{ route('taxpayer.show', $taxpayer->id ) }}" class="pl-2">
                                                <x-see-svg/>                                          
                                            </x-link-button>
                                            @endcan
                                            @can('edit taxpayer')
                                            <x-link-button href="{{ route('taxpayer.edit', $taxpayer->id ) }}" class="pl-2">
                                                <x-edit-svg/>                                          
                                            </x-link-button>
                                            @endcan
                                            @can('delete taxpayer')
                                            <div class="btn p-0">
                                                <form method="POST"
                                                    action="{{ route('taxpayer.destroy', ['taxpayer' => $taxpayer]) }}"
                                                    class=""
                                                >
                                                    @csrf 
                                                    @method('DELETE')  
                                                    <x-danger-button type="submit">
                                                        <x-delete-svg/>
                                                    </x-danger-button>
                                                </form>
                                            </div>
                                            @endcan
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th> {{__('taxpayers.docType')}} </th>
                                    <th> {{__('taxpayers.docNumber')}} </th>
                                    <th> {{__('taxpayers.names')}} </th>
                                    <th> {{__('taxpayers.lastnames')}} </th>
                                    <th> {{__('taxpayers.phone')}} </th>
                                    <th> Actions </th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <x-slot name="script">
        @include('taxpayers.scripts-list')
    </x-slot>
</x-app-layout>