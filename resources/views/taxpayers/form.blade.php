<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl leading-tight">
            {{ __('taxpayers.taxpayers') }} / {{ (isset($taxpayer)) ? __('Edit') : __('Add') }}
        </h2>
    </x-slot>
    <section>
        <div class="py-6">
            <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6">
                <div class="p-4 sm:p-8 bg-primary text-white shadow sm:rounded-lg container">

                    <header>
                        <h2 class="text-lg font-medium">
                            {{ __('taxpayers.info') }}
                        </h2>
                    </header>

                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    
                    <hr>

                    @if(isset($taxpayer))
                        <form action="{{ route('taxpayer.storeOrUpdate', ['taxpayer' =>$taxpayer]) }}" method="POST"
                            class="mt-6 space-y-6">
                            @method('PUT')
                            @csrf
                            <input type="hidden" name="id" value="{{$taxpayer->id}}">
                    @else
                        <form action="{{ route('taxpayer.storeOrUpdate') }}" method="POST" class="mt-6 space-y-6">
                            @method('POST')
                            @csrf
                    @endif
                            <input type="hidden" id="read_only" value="{{$read_only ?? 'false'}}">

                            <div class="row">
                                <!-- docType -->
                                <div class="col-6">
                                    <x-input-label for="doc_type" :value="__('taxpayers.docType')" />
                                    <x-select-input :options="$doctype"
                                        :value="isset($taxpayer) ? $taxpayer->doc_type : (old('doc_type') ?? '')"
                                        id="doc_type" name="doc_type" class="mt-1 block w-full"
                                        required autofocus
                                        autocomplete="doc_type"></x-select-input>
                                </div>

                                <!-- docNumber -->
                                <div class="col-6">
                                    <x-input-label for="doc_number" :value="__('taxpayers.docNumber')" />
                                    <x-text-input id="doc_number" name="doc_number" type="text"
                                        class="mt-1 block w-full @error('title') is-invalid @enderror"
                                        :value="isset($taxpayer) ? $taxpayer->doc_number : (old('doc_number') ?? '')"
                                         required autofocus autocomplete="doc_number" />
                                    <x-input-error class="mt-2" :messages="$errors->get('doc_number')" />
                                </div>
                            </div>

                            <div class="row">
                                <!-- names -->
                                <div class="col-6" id="names_div">
                                    <x-input-label for="name" id="label_names" :value="__('taxpayers.names')" />
                                    <x-text-input id="names" name="names" type="text" class="mt-1 block w-full"
                                        :value="isset($taxpayer) ? $taxpayer->names : (old('names') ?? '')"
                                         required autofocus autocomplete="names" />
                                    <x-input-error class="mt-2" :messages="$errors->get('names')" />
                                </div>

                                <!-- lastnames -->
                                <div class="col-6">
                                    <x-input-label for="lastnames" id="label_lastnames" :value="__('taxpayers.lastnames')" />
                                    <x-text-input id="lastnames" name="lastnames" type="text" class="mt-1 block w-full"
                                        :value="isset($taxpayer) ? $taxpayer->lastnames : (old('lastnames') ?? '')"
                                         autofocus autocomplete="lastnames" />
                                    <x-input-error class="mt-2" :messages="$errors->get('lastnames')" />
                                </div>
                            </div>

                            <div class="row">
                                <!-- address -->
                                <div class="col-12">
                                    <x-input-label for="address" :value="__('taxpayers.address')" />
                                    <x-text-input id="address" name="address" type="text" class="mt-1 block w-full"
                                        :value="isset($taxpayer) ? $taxpayer->address : (old('address') ?? '')" 
                                         required autofocus autocomplete="address" />
                                    <x-input-error class="mt-2" :messages="$errors->get('address')" />
                                </div>
                            </div>

                            <div class="row">
                                <!-- phone -->
                                <div class="col-6">
                                    <x-input-label for="phone" :value="__('taxpayers.phone')" />
                                    <x-text-input id="phone" name="phone" type="text" class="mt-1 block w-full"
                                        :value="isset($taxpayer) ? $taxpayer->phone : (old('phone') ?? '')"
                                         required autofocus autocomplete="phone" />
                                    <x-input-error class="mt-2" :messages="$errors->get('phone')" />
                                </div>
                                <!-- secondary phone -->
                                <div class="col-6">
                                    <x-input-label for="secondary_phone" :value="__('taxpayers.secondary_phone')" />
                                    <x-text-input id="secondary_phone" name="secondary_phone" type="text" class="mt-1 block w-full"
                                        :value="isset($taxpayer) ? $taxpayer->secondary_phone : (old('secondary_phone') ?? '')"
                                         autofocus autocomplete="secondary_phone" />
                                    <x-input-error class="mt-2" :messages="$errors->get('secondary_phone')" />
                                </div>
                            </div>

                            <div class="row">
                                <!-- email -->
                                <div>
                                    <x-input-label for="email" :value="__('Email')" />
                                    <x-text-input id="email" name="email" type="email" class="mt-1 block w-full"
                                        :value="isset($taxpayer) ? $taxpayer->email : (old('email') ?? '')"
                                         required autocomplete="username" />
                                    <x-input-error class="mt-2" :messages="$errors->get('email')" />
                                </div>
                            </div>

                            <div class="row">
                                <div class="flex items-center gap-4 justify-content-end">
                                    <x-secondary-button onclick="history.back()" class="bg-light">
                                        {{ __('Cancel') }}
                                    </x-secondary-button>

                                    @if(!(isset($read_only) && $read_only))
                                    <x-primary-button>
                                        @if (isset($taxpayer))
                                        {{ __('Update') }}
                                        @else
                                        {{ __('Save') }}
                                        @endif
                                    </x-primary-button>
                                    @endif

                                    @if (session('status') === 'profile-updated')
                                    <p x-data="{ show: true }" x-show="show" x-transition
                                        x-init="setTimeout(() => show = false, 2000)"
                                        class="text-sm text-gray-600 dark:text-gray-400">{{ __('Saved.') }}</p>
                                    @endif
                                </div>
                            </div>

                        </form>
                </div>
            </div>
        </div>
    </section>

    <x-slot name="script">
        @include('taxpayers.scripts-form')
    </x-slot>

</x-app-layout>