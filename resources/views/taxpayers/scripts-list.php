<script>
   const userRole = document.getElementById('role').value;

   function confirmDelete(event) {
      event.preventDefault();
      Swal.fire({
         title: 'Are you sure?',
         text: "You won't be able to revert this action!",
         icon: 'warning',
         showCancelButton: true,
         confirmButtonColor: '#d33',
         cancelButtonColor: '#33d',
         confirmButtonText: 'Yes, delete it!',
         cancelButtonText: 'Cancel'
      }).then((result) => {
         if (result.isConfirmed) {
            document.querySelector('form').submit();
         }
      });
   }


</script>

<script type="module">

   $('#taxpayersTable').DataTable({
      language,
      columns: [
         { data: "docType", width: '10%' },
         { data: "docNumber", width: '15%' },
         { data: "names", width: '20%' },
         { data: "lastnames", width: '20%' },
         { data: "phone", width: '15%' },
         { data: "actions", width: '20%' },
      ],
   });
   
</script>
   {{--<!-- //{ data: "actions", visible: (($('#role').val() == '["Normal"]') ? false : true), width: '20%' }, -->--}}