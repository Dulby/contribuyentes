@props(['options', 'value'])

<select     {!! $attributes->merge([
    'class' => 'form-select
            shadow-sm'
    ]) !!}
>
    @foreach ($options as $key => $label)
        <option 
            value="{{ $key }}"
            @if ($value == $key)
                selected 
            @endif
        >
            {{ $label }}
        </option>
    @endforeach
</select>