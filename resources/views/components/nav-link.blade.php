@props(['active'])

@php
$classes = 'inline-flex
            items-center
            no-underline
            px-1
            pt-1
            text-sm
            font-medium
            transition
            duration-150
            ease-in-out
            leading-5
            ';
$classes .= ($active ?? false)
            ? ' text-white'
            : ' text-body-tertiary';
@endphp

<a {{ $attributes->merge(['class' => $classes]) }}>
    {{ $slot }}
</a>
