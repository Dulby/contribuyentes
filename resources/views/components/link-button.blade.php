@props(['active'])

@php
$classes = 'btn bg-dark shadow';

$classes .= ($active ?? false)
            ? ' active'
            : ' ';

@endphp

<a {{ $attributes->merge(['class' => $classes]) }} >
    {{ $slot }}
</a>
