@props(['status'])

@if ($status)
    <div {{ $attributes->merge(['class' => 'font-medium text-sm text-sucess-600 dark:text-sucess-400']) }}>
        {{ $status }}
    </div>
@endif
