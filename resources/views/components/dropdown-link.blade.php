<a 
{{ $attributes->merge([
    'class' => 'block
                w-full
                text-white
                no-underline
                px-4
                py-2
                text-start
                text-sm
                leading-5
                transition
                duration-150
                ease-in-out
                ']) }}
>
    {{ $slot }}
</a>
