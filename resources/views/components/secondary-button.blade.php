<button {{ $attributes->merge([
    'type' => 'button',
    'class' => 'btn btn-light shadow'
    ]) }}>
    {{ $slot }}
</button>
