<button {{ $attributes->merge([
    'type' => 'submit',
    'class' => 'btn btn-info shadow'
    ]) }}>
    {{ $slot }}
</button>
