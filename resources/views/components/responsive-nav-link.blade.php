@props(['active'])

@php
$classes = 'nav-link
            block
            w-full
            ps-3
            pe-4
            py-2
            text-start
            text-base
            font-medium
            transition
            duration-150
            ease-in-out';

$classes .= ($active ?? false)
            ? ' active'
            : ' text-body-tertiary';
@endphp

<a
    {{ $attributes->merge(['class' => $classes]) }}
>
    {{ $slot }}
</a>
