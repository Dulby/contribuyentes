
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl leading-tight">
            {{ __('docTypes.docTypes') }} / {{ (isset($docType)) ? __('Edit') : __('Add') }}
        </h2>
    </x-slot>
    <section>
        <div class="py-6">
            <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6">
                <div class="p-4 sm:p-8 bg-primary text-white shadow sm:rounded-lg container">
                    <header>
                        <h2 class="text-lg font-medium text-white">
                            {{ __('docTypes.info') }}
                        </h2>
                    </header>
                    
                    @can('create doctype')
                    <div class="d-flex justify-content-end">
                        <x-link-button href="{{ route('taxpayer.create') }}">
                            {{ __('Add') }}
                        </x-link-button>
                    </div>
                    @endcan

                    <hr>
                    
                    <div class="py-6">
                    <input type="hidden" id="role" value="{{ Auth::user()->getRoleNames() }}" >

                        <table id="docTypesTable" class="table table-striped table-secondary" style="width:100%">
                            <thead>
                                <tr>
                                    <th> {{__('docTypes.id')}} </th>
                                    <th> {{__('docTypes.short_name')}} </th>
                                    <th> {{__('docTypes.name')}} </th>
                                    <th> {{__('Actions')}} </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($documentTypes as $docType)
                                <tr>
                                    <td> {{ $docType->id }} </td>
                                    <td> {{ $docType->short_name }} </td>
                                    <td> {{ $docType->name }} </td>
                                    <td>
                                        <div class="d-flex justify-content-between align-items-center">

                                            @can('view doctype')
                                            <x-link-button href="{{ route('document-type.show', $docType->id ) }}" class="pl-2">
                                                <x-see-svg/>                                          
                                            </x-link-button>
                                            @endcan

                                            @can('edit doctype')
                                            <x-link-button href="{{ route('document-type.edit', $docType->id ) }}">
                                                <x-edit-svg/>                                          
                                            </x-link-button>
                                            @endcan

                                            @can('delete doctype')
                                            <form method="POST" 
                                                action="{{ route('document-type.destroy', ['document_type' => $docType]) }}"
                                                class="ml-2"
                                            >
                                                @csrf 
                                                @method('DELETE')  
                                                <x-danger-button type="submit">
                                                    <x-delete-svg/>
                                                </x-danger-button>
                                            </form>
                                            @endcan
                                        </div>                                          
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <x-slot name="script">
        @include('doc-type.scripts-list')
    </x-slot>
</x-app-layout>
