<script type="module">
    $('#docTypesTable').DataTable({
        language,
        columns: [
            { width: '20%' },
            { width: '20%' },
            { width: '40%' },
            { width: '20%', visible: (($('#role').val() !== '["SuperAdmin"]' ) ? false : true) }
        ],
    });
</script>

