<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl leading-tight">
            {{ __('user.users') }}
        </h2>
    </x-slot>
    <section>
        <div class="py-12">
            <div class=" pt-3 max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6">
                <div class="p-4 sm:p-8 bg-primary text-white shadow sm:rounded-lg">
                    <header>
                        <h2 class="text-lg font-medium">
                            {{ __('user.users') }}
                        </h2>
    
                        {{--<!-- <p class="mt-1 text-sm">
                            {{ __("Update your account's profile information and email address.") }}
                        </p> -->--}}
                    </header>
                    @can('create user')
                    <div class="d-flex justify-content-end">
                        <x-link-button href="{{ route('user.create') }}">
                            {{ __('Add') }}
                        </x-link-button>
                    </div>
                    @endcan

                    <div class="py-6">
                        <input type="hidden" id="role" value="{{ Auth::user()->getRoleNames() }}" >
                        <hr>
                        <!-- <table -->
                        <!--   Tipo documento | Documento | Nombres | Apellidos -->
                        <table id="usersTable" class="table table-striped table-secondary" style="width:100%">
                            <thead>
                                <tr>
                                    <th> {{__('user.name')}} </th>
                                    <th> {{__('user.lastname')}} </th>
                                    <th> {{__('user.email')}} </th>
                                    <th> {{__('user.updated_at')}} </th>
                                    <th> {{__('Actions')}} </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                <tr>
                                    <td> {{ $user->name }} </td>
                                    <td> {{ $user->lastname }} </td>
                                    <td> {{ $user->email }} </td>
                                    <td> {{ date("d F Y : h m A", strtotime($user->updated_at)) }} </td>
                                    <td>
                                        <div class="d-flex justify-content-between align-items-center">
                                            @can('view doctype')
                                            <x-link-button href="{{ route('user.show', $user->id ) }}" class="pl-2">
                                                <x-see-svg/>                                          
                                            </x-link-button>
                                            @endcan
                                            @can('edit user')
                                            <x-link-button href="{{ route('user.edit', $user->id ) }}" class="pl-2">
                                                <x-edit-svg/>                                          
                                            </x-link-button>
                                            @endcan
                                            @can('delete user')
                                            <div class="btn p-0">
                                                <form method="POST"
                                                    action="{{ route('user.destroy', ['user' => $user]) }}"
                                                    class=""
                                                >
                                                    @csrf 
                                                    @method('DELETE')  
                                                    <x-danger-button type="submit">
                                                        <x-delete-svg/>
                                                    </x-danger-button>
                                                </form>
                                            </div>
                                            @endcan
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th> {{__('user.name')}} </th>
                                    <th> {{__('user.lastname')}} </th>
                                    <th> {{__('user.email')}} </th>
                                    <th> {{__('user.updated_at')}} </th>
                                    <th> Actions </th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <x-slot name="script">
        @include('users.scripts-list')
    </x-slot>
</x-app-layout>