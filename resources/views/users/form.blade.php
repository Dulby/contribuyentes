<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl leading-tight">
            {{ __('user.users') }} / {{ (isset($user)) ? __('Edit') : __('Add') }}
        </h2>
    </x-slot>
    <section>
        <div class="py-6">
            <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6">
                <div class="p-4 sm:p-8 bg-primary text-white shadow sm:rounded-lg container">

                    <header>
                        <h2 class="text-lg font-medium">
                            {{ __('user.info') }}
                        </h2>
                    </header>

                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    <hr>

                    @if(isset($user))
                    <form action="{{ route('user.storeOrUpdate', ['user' =>$user]) }}" method="POST"
                        class="mt-6 space-y-6">
                        @method('PUT')
                        @csrf
                        <input type="hidden" name="id" value="{{$user->id}}">
                    @else
                    <form action="{{ route('user.storeOrUpdate') }}" method="POST" class="mt-6 space-y-6">
                        @method('POST')
                        @csrf
                    @endif
                        <input type="hidden" id="read_only" value="{{$read_only ?? 'false' }}">

                        <div class="container">
                            <div class="row">
                                <div class="col-9">
                                    <div class="row">
                                        <!-- Name -->
                                        <div class="col-6">
                                            <x-input-label for="name" :value="__('Name')" />
                                            <x-text-input id="name" name="name" type="text"
                                                class="block mt-1 w-full"
                                                :value="isset($user) ? $user->name : (old('name') ?? '')"
                                                autofocus autocomplete="name" />
                                            <x-input-error :messages="$errors->get('name')" class="mt-2" />
                                        </div>

                                        <!-- Surnames -->
                                        <div class="col-6">
                                            <x-input-label for="lastname" :value="__('Lastname')" />
                                            <x-text-input id="lastname" name="lastname" type="text"
                                                class="block mt-1 w-full"
                                                :value="isset($user) ? $user->lastname: (old('lastname') ?? '')"
                                                autofocus autocomplete="lastname" />
                                            <x-input-error :messages="$errors->get('lastname')"
                                                class="mt-2" />
                                        </div>
                                    </div>

                                    <div class="row">
                                        <!-- Email Address -->
                                        <div class="col-12">
                                            <x-input-label for="email" :value="__('E-Mail Address')" />
                                            <x-text-input id="email" name="email" type="email"
                                                class="block mt-1 w-full"
                                                :value="isset($user) ? $user->email : (old('email') ?? '')"
                                                autocomplete="username" />
                                            <x-input-error :messages="$errors->get('email')" class="mt-2" />
                                        </div>
                                    </div>

                                    <div class="row">
                                        <!-- Password -->
                                        <div class="col-6">
                                            <x-input-label for="password" :value="__('Password')" />
                                            <x-text-input id="password" name="password" type="password"
                                                class="block mt-1 w-full"
                                                :value="isset($user) ? $user->password : (old('password') ?? '')"
                                                autocomplete="new-password" />
                                            <x-input-error :messages="$errors->get('password')"
                                                class="mt-2" />
                                        </div>

                                        <!-- Confirm Password -->
                                        <div class="col-6">
                                            <x-input-label for="password_confirmation"
                                                :value="__('Confirm Password')" />
                                            <x-text-input id="password_confirmation"
                                                name="password_confirmation" type="password"
                                                class="block mt-1 w-full"
                                                :value="isset($user) ? $user->password : (old('password') ?? '')"
                                                autocomplete="new-password" />
                                            <x-input-error :messages="$errors->get('password_confirmation')"
                                                class="mt-2" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-3 d-flex justify-content-center align-items-center">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5>Roles</h5>
                                        </div>
                                        <div class="card-body">
                                            <p>SuperAdmin</p>
                                            <p>Admin</p>
                                            <p>Normal</p>
                                        </div>
                                        <div class="card-footer">
                                            EN PROCESO AÚN
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>


                        <div class="row">
                            <div class="flex items-center gap-4 justify-content-end">
                                <x-secondary-button onclick="history.back()" class="bg-light">
                                    {{ __('Cancel') }}
                                </x-secondary-button>

                                @if(!(isset($read_only) && $read_only))
                                <x-primary-button>
                                    @if (isset($taxpayer))
                                    {{ __('Update') }}
                                    @else
                                    {{ __('Save') }}
                                    @endif
                                </x-primary-button>
                                @endif

                                @if (session('status') === 'profile-updated')
                                <p x-data="{ show: true }" x-show="show" x-transition
                                    x-init="setTimeout(() => show = false, 2000)"
                                    class="text-sm text-gray-600 dark:text-gray-400">{{ __('Saved.') }}
                                </p>
                                @endif
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </section>

    <x-slot name="script">
        @include('taxpayers.scripts-form')
    </x-slot>

</x-app-layout>