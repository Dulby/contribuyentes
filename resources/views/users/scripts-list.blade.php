<script type="module">
    $('#usersTable').DataTable({
        language,
        columns: [
            { data: "name", width: '10%' },
            { data: "lastname", width: '15%' },
            { data: "email", width: '20%' },
            { data: "updated_ad", width: '20%' },
            { data: "actions", width: '20%' },
        ],
    });

</script>