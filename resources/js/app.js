import "bootswatch/dist/minty/bootstrap.min.css";
import './bootstrap';

import jQuery from "jquery";
window.$ = jQuery;

import DataTable from "datatables.net-dt";
window.DataTable = DataTable;

import language from 'datatables.net-plugins/i18n/es-CO.mjs';
window.language = language;

import Alpine from 'alpinejs';
window.Alpine = Alpine;

Alpine.start();
