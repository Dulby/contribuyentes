<?php

return [
    'users' => 'Users',
    'name' => 'Name',
    'lastname' => 'Lastname',
    'email' => 'Email',
    'updated_at' => 'Last Update',
    'password' => 'Password',
    'confirm_password' => 'Confirm Password',
    'Profile' => 'Profile',
    'user' => 'User',
    'info' => 'User Information'
];