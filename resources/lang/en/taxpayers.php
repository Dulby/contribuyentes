<?php

return [
    'taxpayers' => 'Taxpayers',
    'docType' => 'Document Type',
    'docNumber' => 'Document Number',
    'names' => 'Names',
    'lastnames' => 'Surnames',
    'address' => 'Address',
    'phone' => 'Phone',
    'secondary_phone' => 'Secondary Phone',
    'email' => 'Email',
    'birthdate' => 'Birthdate',
    'info' => 'Taxpayer Information',
    'taxpayer' => 'Taxpayer',
    'business_name' => 'Business name',
    
];