<?php

return [
    'profile' => 'Profile',
    'profile_information' => "Profile Information",
    'profile_information_explication' => "Update your account's profile information and email address.",
];
?>