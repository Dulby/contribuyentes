<?php

return [
    'users' => 'Usuarios',
    'name' => 'Nombre',
    'lastname' => 'Apellido',
    'email' => 'Correo Electrónico',
    'updated_at' => 'Última Actualización',
    'password' => 'Contraseña',
    'confirm_password' => 'Confirmar contraseña',
    'Profile' => 'Perfil',
    'user' => 'Usuario',
    'info' => 'Información del Usuario'
];
