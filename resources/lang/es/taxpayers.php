<?php

return [
    'taxpayers' => 'Contribuyentes',
    'docType' => 'Tipo de documento',
    'docNumber' => 'Número de documento',
    'names' => 'Nombres',
    'lastnames' => 'Apellidos',
    'address' => 'Dirección',
    'phone' => 'Teléfono',
    'secondary_phone' => 'Teléfono Secundario',
    'email' => 'Correo electrónico',
    'birthdate' => 'Birthdate',
    'info' => 'Información del Contribuyente',
    'taxpayer' => 'Taxpayer',
    'business_name' => 'Razon Social'
    
];