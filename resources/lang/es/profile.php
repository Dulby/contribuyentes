<?php

return [
    'profile' => 'Perfil',
    'profile_information' => "Información del Perfil",
    'profile_information_explication' => "Actualice la información del perfil y la dirección de correo electrónico de su cuenta.",
];
?>]